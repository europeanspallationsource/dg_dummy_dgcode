#include "Core/Python.hh"
#include <iostream>

void somecppfunc()
{
  std::cout<<"in somecppfunc in BasicExamples.anothermodule"<<std::endl;
}

PYTHON_MODULE
{
  py::def("somecppfunc", somecppfunc, "This is some C++ function");
}
