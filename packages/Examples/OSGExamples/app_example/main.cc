#include "Core/FindData.hh"

#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgViewer/CompositeViewer>
#include <osgViewer/View>

osgViewer::View* createView( int x, int y, int w, int h, osg::Node* scene )
{
  osg::ref_ptr<osgViewer::View> view = new osgViewer::View;
  view->setSceneData( scene );
  view->setUpViewInWindow( x, y, w, h );
  return view.release();
}

int main( int, char**  )
{
  //argument parsing:

  //osg::ArgumentParser arguments( &argc, argv );
  //osg::ref_ptr<osg::Node> root = osgDB::readNodeFiles( arguments );
  //viewer.setSceneData( root.get() );

  //std::string filename;
  //arguments.read( "--model", filename );
  //osg::ref_ptr<osg::Node> root = osgDB::readNodeFile( filename );

  osg::ref_ptr<osg::Node> model1 = osgDB::readNodeFile(Core::findData("OSGExamples","glider.osg"));

  if (true) {
    //full-screen:
    osgViewer::Viewer viewer;
    viewer.setSceneData(model1);
    return viewer.run();
  } else {
    //windowed (add multiple View instances for multiple windows)
    osgViewer::View* view1 = createView(50, 50, 320, 240, model1);
    osgViewer::CompositeViewer viewer;
    viewer.addView( view1 );
    return viewer.run();
  }

}
