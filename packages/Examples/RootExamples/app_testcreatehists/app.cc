#include "TH1F.h"
#include "TFile.h"

int main(int,char**)
{
  TH1F h("a test hist","testhist",100,0.0,100.0);
  h.Fill(17.0);
  TFile f("hist_fromcpp.root","RECREATE");
  h.Print("all");
  h.Write();
  f.Close();
  return 0;
}
