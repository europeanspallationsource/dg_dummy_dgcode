#include "Core/FPE.hh"

//TODO: Enable on OSX if certain conditions are met or if the compiler is recent enough?
#if defined(NDEBUG) || defined(__APPLE__) || (__cplusplus<201103L&&__cplusplus!=1)
//do nothing in release build or on osx or if running with older c++
//standard (note that one old platforms __cplusplus might be
//incorrectly defined to be 1, in which case we will assume -std=c++0x
//is present.
void Core::catch_fpe(){}
#else

#include "Core/Assert.hh"
#include <cfenv>
#include <cstring>
#include <csignal>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <execinfo.h>

namespace Core {
  void custom_sigfpe_handler( int sig, siginfo_t *info, void* ) {
    if (sig!=SIGFPE)
      return;

    //protect against secondary SIGFPE's:
    static bool inHandler = false;
    if ( inHandler )
      return;
    inHandler = true;

    //flush pending output:
    std::cout.flush();
    std::cerr.flush();

    //Print diagnostics (TODO: Optionally print the stack-trace):
    int fpe = info->si_code;
    if      (fpe==FPE_INTDIV) printf("ERROR - FPE detected: integer divide by zero\n");
    else if (fpe==FPE_INTOVF) printf("ERROR - FPE detected: integer overflow\n");
    else if (fpe==FPE_FLTDIV) printf("ERROR - FPE detected: floating point divide by zero\n");
    else if (fpe==FPE_FLTOVF) printf("ERROR - FPE detected: floating point overflow\n");
    else if (fpe==FPE_FLTUND) printf("ERROR - FPE detected: floating point underflow\n");
    else if (fpe==FPE_FLTRES) printf("ERROR - FPE detected: floating point inexact result\n");
    else if (fpe==FPE_FLTINV) printf("ERROR - FPE detected: floating point invalid operation\n");
    else if (fpe==FPE_FLTSUB) printf("ERROR - FPE detected: subscript out of range\n");
    else printf("ERROR - FPE detected: unknown fpe\n");

    //Produce backtrace:
    const unsigned traceoffset = 1;//to avoid printing trace from the FPE catcher, but better too small than missing something
    const unsigned tracelimit = 30 + traceoffset;
    void *trace_addr[tracelimit];
    size_t ntrace = backtrace(trace_addr, tracelimit);
    char ** trace_symbs = backtrace_symbols (trace_addr,ntrace);
    for(size_t itrace=traceoffset;itrace<ntrace;++itrace) {
      if (!trace_symbs[0])
        break;
      printf("Backtrace [%i]: %s\n",int(itrace-traceoffset),trace_symbs[itrace]);
    }

    std::cout.flush();
    assert(0&&"SIGFPE detected - invalid mathematical operation");//this will lead to exit code 128+SIGABRT=134 so we use that below as well:
    std::exit(128+SIGABRT);
  }
}

void Core::catch_fpe() {
  static bool first = true;
  if (!first)
    return;
  first = false;
  struct sigaction sigact, initial_sa;
  std::memset (&sigact, 0, sizeof(sigact));
  sigact.sa_sigaction = Core::custom_sigfpe_handler;
  sigemptyset(&sigact.sa_mask);
  sigact.sa_flags = SA_SIGINFO;
  feenableexcept(FE_DIVBYZERO|FE_INVALID);
  //We do not enable FE_INEXACT and FE_UNDERFLOW or FE_OVERFLOW since they are
  //too easy to trigger (FE_OVERFLOW can be triggered by DBL_MAX*a for a>1).
  int ret = sigaction(SIGFPE, &sigact, &initial_sa);
  if ( ret!=0 )
    printf("Core::FPE ERROR: Could not install FPE handler.\n");
}
#endif
