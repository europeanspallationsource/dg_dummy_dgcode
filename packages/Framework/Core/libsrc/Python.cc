#include "Core/Python.hh"
#include <stdexcept>

bool py::isPyInit()
{
  return Py_IsInitialized();
}

void py::pyInit(py::list sysargv)
{
  if (isPyInit())
    throw std::runtime_error("Attempt at initialising Python interpreter twice detected");
  Py_Initialize();
  py::import("sys").attr("argv")=sysargv;
}

void py::pyInit(const char * argv0) {
  py::list pysysargv;
  //Always put at least a dummy entry for sys.argv[0], since some python modules
  //will assume this is always present (for instance the matplotlib tkinter
  //backend with python 3.7):
  pysysargv.append(std::string(argv0?argv0:"dummyargv0"));
  pyInit(pysysargv);
}

void py::pyInit(int argc, char** argv)
{
  py::list pysysargv;
  for (int i = 0; i < argc; ++i)
    pysysargv.append(std::string(argv[i]));
  pyInit(pysysargv);
}

void py::ensurePyInit() {
  if (!isPyInit())
    pyInit();
}

