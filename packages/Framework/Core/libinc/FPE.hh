#ifndef Core_FPE_hh
#define Core_FPE_hh

namespace Core {

  //If not in release builds, a call to the following function will install a
  //signal handler for SIGFPE which will print diagnostics, assert(false) and
  //exit the process with non-zero exit code of 134. Calling it multiple times
  //has no further effect.

  void catch_fpe();

}

#endif
