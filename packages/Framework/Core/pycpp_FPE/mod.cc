#include "Core/FPE.hh"
#include "Core/Python.hh"

PYTHON_MODULE
{
  py::def("catch_fpe",&Core::catch_fpe);
}
