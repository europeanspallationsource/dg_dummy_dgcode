#!/bin/bash

if [ "x$DGCODE_USEPY3" != "x1" ]; then
    if [ "x$DGCODE_USEPY2" == "x1" ]; then
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  WARNING WARNING:                              !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  User has set DGCODE_USEPY2=1, so I will try   !!!!"
        echo "!!!!  to use legacy python2 instead of python3.     !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  This is not really supported and the option   !!!!"
        echo "!!!!  is likely to go away completely in the very   !!!!"
        echo "!!!!  near future!!                                 !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  It is highly recommended to undo and run:     !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  unset DGCODE_USEPY2; . bootstrap.sh           !!!!"
        echo "!!!!                                                !!!!"
        #echo "!!!!  Note that doing so will replace your python3  !!!!"
        #echo "!!!!  command with a fake one that is really        !!!!"
        #echo "!!!!  python2. Very hacky!                         !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  Please let thomas.kittelmann@esss.se know     !!!!"
        echo "!!!!  immediately if you think you have reasons     !!!!"
        echo "!!!!  for using dgcode with python2 for longer      !!!!"
        echo "!!!!  than ~mid June 2019.                          !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo
        sleep 2
        which python2 >&/dev/null
        if [ $? != 0 ]; then
            echo "ERROR: python2 command not found!!!!"
            return 1
        fi
    else
        DGCODE_USEPY3=1
        export DGCODE_USEPY3=1
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  Important notice:                             !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  dgcode now use Python3 by default and the     !!!!"
        echo "!!!!  obsolete Python2 is no longer supported.      !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  If you have an urgent short-term need to      !!!!"
        echo "!!!!  stick with Python2, you can try to see if     !!!!"
        echo "!!!!  it still works for you by running:            !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!  unset DGCODE_USEPY3                           !!!!"
        echo "!!!!  export DGCODE_USEPY2=1                        !!!!"
        echo "!!!!  . bootstrap.sh                                !!!!"
        echo "!!!!                                                !!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo
        sleep 2
    fi
fi

for tmp in DGCODE_DIR ESS_INSTALL_PREFIX; do
    #Cleanout leftover from previous invocations:
    if [ "x${!tmp}" != x ]; then
        function dgcode_prunepath() {
            P=$(IFS=:;for p in ${!1}; do [[ $p != ${2}* ]] && echo -n ":$p"; done)
            export $1=${P:1:99999}
        }
        dgcode_prunepath PYTHONPATH "${!tmp}"
        dgcode_prunepath PATH "${!tmp}"
        dgcode_prunepath LD_LIBRARY_PATH "${!tmp}"
        dgcode_prunepath DYLD_LIBRARY_PATH "${!tmp}"
        unset dgcode_prunepath
        unset ${tmp}
    fi
done

#Where are we?:
DGCODE_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ ! -x $DGCODE_DIR/.system/bin/dgbuild ]; then
    echo "Error: Could not find the dgbuild script"
    unset DGCODE_DIR
    return 1
fi

if [ -f $HOME/.dgcode_prebootstrap.sh ]; then
    source $HOME/.dgcode_prebootstrap.sh
fi

if [ "x$DG_DONT_SET_PYTHONUNBUFFERED" != x1 ]; then
    #make sure all print statements from python are unbuffered, otherwise they
    #will get interspersed with C++/C printouts in context-dependent order in
    #python3:
    export PYTHONUNBUFFERED=1
fi

#Detect clusters with special support:
DG_PLATFORM=
if [[ -d /mnt/groupdata ]] && [[ "x$HOSTNAME" = *.esss.dk ]]; then
    DG_PLATFORM=dmsc
elif [[ -d /afs ]] && [[ "x$HOSTNAME" = *.cern.ch ]] && [[ -f /etc/centos-release ]]; then
    DG_PLATFORM=lxplus
fi
if [ "x$DG_IGNORE_PLATFORM" == x1 ]; then
    DG_PLATFORM=
fi

if [ "x$DG_PLATFORM" != "x" ]; then
    echo
    echo "Detected platform with special support: $DG_PLATFORM"
fi

if [ "x$DGCODE_USEPY3" != "x1" ]; then
    #Start from clean slate!
    if [ "x$DGDEPDIR" != "x" -a -f "$DGDEPDIR/unsetup.sh" ]; then
        echo
        echo "-> Deactivating python3 installation at $DGDEPDIR since DGCODE_USEPY2=1."
        echo
        . "$DGDEPDIR/unsetup.sh"
    fi


fi

if [[ "x$DG_PLATFORM" == xdmsc ]]; then
    unset DG_PLATFORM
    if [ "x$DGCODE_USEPY3" == "x1" ]; then
        #unload all modules, get gcc 8.20, python3.5, a venv with common
        #modules, new cmake, etc:
        . /mnt/groupdata/detector/dgcode_installs/centos7_gcc820_py35_dgdepfixer/setup.sh
        #This next one does not work yet (missing C++ header) but at least it fails
        #at configuration rather than build stage unlike the system-wide one. So it
        #is better to enable it so users won't have to specify HDF5=0 manually:
        module load hdf5/1.10.1
    else
        #Fix modules
        echo
        echo "  => Using DMSC modules to get new gcc, python, etc. (type 'module list' to inspect)"
        [ -v LOADEDMODULES -a -n "$LOADEDMODULES" ] && module unload `echo $LOADEDMODULES|tr ':' ' '`
        #module unload intel gcc boost hdf5 root geant4 cmake anaconda >&/dev/null
        if [ "x$DGCODE_USEPY3" == "x1" ]; then
            module load gcc/8.2.0
            source /mnt/groupdata/detector/dgcode_installs/centos7_gcc820/venv_py35/setup.sh
        else
            module load anaconda/2.5.0 gcc/8.2.0
        fi
        #This next one does not work yet (missing C++ header) but at least it fails
        #at configuration rather than build stage unlike the system-wide one. So it
        #is better to enable it so users won't have to specify HDF5=0 manually:
        module load hdf5/1.10.1
        #TODO: Use hdf5/1.10.2 instead - but needs fixing for gcc 4.8.2!!!
        #Might be missing mercurial, use custom:
        #DG_TMP=/mnt/groupdata/detector/dgcode_installs/centos7/pip-mercurial-4.4.2
        #which hg > /dev/null 2>&1
        #if [ $? != 0 -a -f $DG_TMP/hghackbin/hg ]; then
        #    #temporary (?) workaround to provide mercurial on new centos queues at DMSC:
        #    export PATH="$DG_TMP/hghackbin:$PATH"
        #    echo "  => Picking up custom mercurial from $DG_TMP"
        #fi
        #DG_TMP=/mnt/groupdata/detector/dgcode_installs/centos7/emacs-25.3
        #which emacs > /dev/null 2>&1
        #if [ $? != 0 -a -d $DG_TMP/dgbinhack ]; then
        #    export PATH="$DG_TMP/dgbinhack:$PATH"
        #    echo "  => Picking up custom emacs from $DG_TMP"
        #fi
        DG_TMP=/mnt/groupdata/detector/dgcode_installs/centos7_gcc820/cmake-3.13.4
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom cmake from $DG_TMP"
        fi
        DG_TMP=/mnt/groupdata/detector/dgcode_installs/centos7_gcc820/geant4-10.04.p03
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom geant4 from $DG_TMP"
        fi
        DG_TMP=/mnt/groupdata/detector/dgcode_installs/centos7_gcc820/root-6.14.04
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom root from $DG_TMP"
        fi
    fi
    unset DG_TMP
fi

if [[ "x$DG_PLATFORM" == xlxplus ]]; then
    unset DG_PLATFORM
    if [ "x$DGCODE_USEPY3" == "x1" ]; then
        #unload all modules, get new gcc 8.20, a venv with common
        #py3 modules, new cmake, etc:
        . /afs/cern.ch/work/t/tkittel/public/dginstalls_w_devtools8/setup.sh
    else
        if [ -f /usr/bin/scl -a -f /etc/scl/prefixes/devtoolset-8 ]; then
            if [[ "xx $X_SCLS xx" != *" devtoolset-8 "* ]]; then
                echo "  => Enabling devtoolset-8 SCL"
                source scl_source enable devtoolset-8
            fi
        else
            echo "  => ERROR: Could not find devtoolset-8 SCL. Did lxplus setup evolve again?"
        fi
        if [ "x$DGCODE_USEPY3" == "x1" ]; then
            if [ -f /usr/bin/scl -a -f /etc/scl/prefixes/rh-python36 ]; then
                if [[ "x $X_SCLS x" != *" rh-python36 "* ]]; then
                    echo "  => Enabling rh-python36 SCL"
                    source scl_source enable rh-python36
                fi
            else
                echo "  => ERROR: Could not find rh-python36 SCL. Did lxplus setup evolve again?"
            fi
            DG_TMP=/afs/cern.ch/work/t/tkittel/public/venv_py36
            if [ -f $DG_TMP/setup.sh ]; then
                . $DG_TMP/setup.sh
                echo "  => Picking up virtual python environment (venv) from $DG_TMP"
            fi
        else
            if [ -f /usr/bin/scl -a -f /etc/scl/prefixes/python27 ]; then
                if [[ "x $X_SCLS x" != *" python27 "* ]]; then
                    echo "  => Enabling python27 SCL"
                    source scl_source enable python27
                fi
            else
                echo "  => ERROR: Could not find python27 SCL. Did lxplus setup evolve again?"
            fi
        fi
        DG_TMP=/afs/cern.ch/work/t/tkittel/public/installs_centos7wdevtools8/cmake-3.13.4
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom cmake from $DG_TMP"
        fi
        DG_TMP=/afs/cern.ch/work/t/tkittel/public/installs_centos7wdevtools8/geant4-10.04.p03_nogdml
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom geant4 from $DG_TMP"
        fi
        DG_TMP=/afs/cern.ch/work/t/tkittel/public/installs_centos7wdevtools8/root-6.14.06_nogdml_nofortran_nogui
        if [ -f $DG_TMP/setup.sh ]; then
            . $DG_TMP/setup.sh
            echo "  => Picking up custom root from $DG_TMP"
        fi
    fi
    unset DG_TMP
fi

if [ "x$DGCODE_USEPY3" == "x1" ]; then
    which python3 > /dev/null 2>&1
    if [ $? != 0 ]; then
        echo "ERROR: You have no python3 installed."
        return 2
    fi
    /usr/bin/env python3 -c '' > /dev/null 2>&1
    if [ $? != 0 ]; then
        echo "ERROR: You seem to have issues such as looping python3 symlinks in your path (\"/usr/bin/env python3\" fails). Some installation is broken!"
        ( IFS=:; for i in $PATH ; do if [ -d $i ]; then readlink -f $i/python3 >/dev/null || echo "NB: Circular broken python3 link found in: $i"; fi; done )
        return 2
    fi
    #Make sure "compiled" dgdepfixer is up to date:
    python3 "$DGCODE_DIR"/.system/depfix/compile.py || echo "ERRORS detected during dgdepfixer compilation!!!!"
    #Inject it into the path:
    export PATH="$DGCODE_DIR/.system/depfix/bld/bin:$PATH"
    #Default DGDEPDIR to ~/dgdepfixer_install unless user already sourced another:
    DG_TMP="$HOME/dgdepfixer_install"
    if [ "x$DGDEPDIR" != "x" ]; then
        DG_TMP="$DGDEPDIR"
    fi
    if [ -f "$DG_TMP/setup.sh" ]; then
        dgdepfixer --checkcompat "$DG_TMP"
        if [ $? == 0 ]; then
            if [ "x$DGDEPDIR" != "x" ]; then
                echo
                echo "-> Confirmed compatibility of active dgdepfixer installation at $DGDEPDIR"
                echo
            else
                echo
                echo "-> Activating installation at ~/dgdepfixer_install/"
                echo
                source $HOME/dgdepfixer_install/setup.sh
                #Reinject dgdepfixer from .system/depfix to head of PATH:
                export PATH="$DGCODE_DIR/.system/depfix/bld/bin:$PATH"
            fi
        else
            if [ "x$DGDEPDIR" != "x" ]; then
                #FIXME: We should have a dgdepfixer --cheapupgradeexisting: leave extras, update (unsetup.sh), dgdepfixer, pymods (via pip calls), cmake (if needed)
                echo "WARNING: Currently active dgdepfixer installation at $DGDEPDIR is incompatible. Please run dgdepfixer and try to recreate it."
                return 2
            else
                echo
                echo "WARNING: Ignoring incompatible or incomplete installation in ~/dgdepfixer_install/ (you should run dgdepfixer to investigate)"
                echo
            fi
        fi
    fi
    dgdepfixer --diagnose
    DG_TMP=$?
    if [ $DG_TMP == 99 ]; then
        unset DG_TMP
        if [ "x$DGCODE_NODEPFIX" != "x1" ]; then
            if [ "x$DGDEPDIR" != "x" -a -f "$DGDEPDIR/unsetup.sh" ]; then
                echo
                echo "-> Deactivating installation at $DGDEPDIR"
                echo
                . "$DGDEPDIR/unsetup.sh"
            fi
            #We reinject dgdepfixer from .system/depfix to head of PATH since the unsetup.sh doesn't always work (!bug??? Fixme!) :
            export PATH="$DGCODE_DIR/.system/depfix/bld/bin:$PATH"
            echo
            echo "WARNING: System issues detected. You can try to fix them now with the command:"
            echo
            echo "   dgdepfixer"
            echo
            echo "Alternatively: if you are an expert and think the detected issues are not important, you can ignore this check"
            echo "by 'export DGCODE_NODEPFIX=1'. In any case, you must source the bootstrap.sh file again once done:"
            echo
            echo "  . $DGCODE_DIR/bootstrap.sh"
            echo
            return 2
        else
            echo "Proceeding despite detected problems since DGCODE_NODEPFIX is set."
        fi
    elif [ $DG_TMP != 0 ]; then
        echo "ERROR: Aborting setup due to fundamental platform issues"
        unset DG_TMP
        return 2
    fi
    unset DG_TMP
fi

#Fixme: The diagnose tool should take care of these next many checks, once we are done with python2:
#which hg > /dev/null 2>&1
#if [ $? != 0 ]; then
#    echo "Warning: You have no mercurial installed (hg command not found). This is a bit odd but not a problem if you know what you are doing."
#fi

which cmake > /dev/null 2>&1
if [ $? != 0 ]; then
    echo "Error: You have no CMake installed (cmake command not found). Please rectify."
    unset DG_TMP
    return 2
fi

#Python:
if [ "x$DGCODE_USEPY3" == "x1" ]; then
    DG_TMP=`which python3 2>&1` || DG_TMP=notfound
    if [ "x$DG_TMP" == "xnotfound" ]; then
        echo "Error: You have no python3 installed. Please rectify."
        unset DG_TMP
        return 2
    fi
else
    DG_TMP=`which python2 2>&1` || DG_TMP=notfound
    if [ "x$DG_TMP" == "xnotfound" ]; then
        echo "Error: You have no python installed (no python2 command found). Please rectify."
        unset DG_TMP
        return 2
    fi
fi

if [ "x$DGCODE_USEPY3" == "x1" ]; then
    if [ "x$DGCODE_BOOTSTRAP_OMITCHECK_PYVERSION" == "x" ]; then
        #NB: The python code in the next line is written so it also works in older python without ternary operators:
        $DG_TMP -c 'import sys; v=sys.version_info[0:2];sys.exit({False:1,True:0}[v>=(3,5) and v<(4,0)])'>/dev/null 2>&1
        if [ $? != 0 ]; then
            echo "Error: Python not found in required version. The python version must be at least 3.5 but less than 4.0 (check with python3 -V)."
            unset DG_TMP
            return 2
        fi
    fi
    #Make sure we do not provide the python2-only modules from .system/py23_extrapypath:
else
    if [ "x$DGCODE_BOOTSTRAP_OMITCHECK_PYVERSION" == "x" ]; then
        #NB: The python code in the next line is written so it also works in older python without ternary operators:
        $DG_TMP -c 'import sys; v=sys.version_info[0:2];sys.exit({False:1,True:0}[v==(2,7)])'>/dev/null 2>&1
        if [ $? != 0 ]; then
            echo "Error: Python not found in required version. The python version must be 2.7.x (check with $DG_TMP -V)."
            unset DG_TMP
            return 2
        fi
    fi
    #Attempt to provide builtin python modules for python2 (if missing) - to facilitate python2->python3 migration:
    $DG_TMP -c 'from builtins import str' 2>/dev/null || export PYTHONPATH="${PYTHONPATH}:${DGCODE_DIR}/.system/py23_extrapypath"
fi
unset DG_TMP


if  [[ "$DGCODE_DIR" == /afs/* ]] || [[ "$DGCODE_DIR" == /nfs/* ]] ; then
    #Prevent caching of python bytecode in .pyc/.pyo files when working on
    #network dir. Trying this as a solution after repeatedly encountering broken
    #.pyc files on AFS at CERN lxplus.
    export PYTHONDONTWRITEBYTECODE=1
fi

export DGCODE_DIR=$DGCODE_DIR

#if [ -f $DGCODE_DIR/.hg/hgrc -a ! -f $DGCODE_DIR/.hg/hgrc_dg_included ]; then
#    #Fixme: Phase out ancient!
#    hg version|head|egrep -qs 'version 1|version 2.0|version 2.1|version 2.2'
#    if [ $? == 0 ]; then
#        echo '%include ../.system/mercurial/hgrc.ancient' >> $DGCODE_DIR/.hg/hgrc
#    else
#        echo '%include ../.system/mercurial/hgrc.modern' >> $DGCODE_DIR/.hg/hgrc
#    fi
#    touch $DGCODE_DIR/.hg/hgrc_dg_included
#fi

#Provide dgbuild and dgrun functions. Make sure the installation area's setup
#script is automatically sourced after each dgbuild invocation:

if [ "x$DGCODE_USEPY3" == "x1" ]; then
    #always run under python3 (todo: update shebang in dgbuild and friends and remove forcepy3 PATH hack in next line)
    function dgbuild() {
        python3 $DGCODE_DIR/.system/bin/dgbuild "$@" ; dgec=$?; if [ -f "$DGCODE_DIR/install/setup.sh" ]; then source "$DGCODE_DIR/install/setup.sh"; fi; return $dgec
    }
else
    #Force dgbuild (and subprocesses!) to run with python+python3 meaning python2. THIS IS VERY UGLY AND SHORT TERM!!!!!
    function dgbuild() {
        PATH="$DGCODE_DIR/.system/bin/forcepy2:$PATH" $DGCODE_DIR/.system/bin/dgbuild "$@" ; dgec=$?; if [ -f "$DGCODE_DIR/install/setup.sh" ]; then source "$DGCODE_DIR/install/setup.sh"; fi; return $dgec
    }
fi

function dgrun() {
    if [ "x$#" == "x0" ]; then
        echo "Usage:"
        echo
        echo "dgrun <program> [args]"
        echo
        echo "Runs \"dgbuild > /dev/null\" and if it finishes successfully it"
        echo "proceeds to launch <program> within the dgcode runtime environment"
        return 1
    fi
    prog=$1
    dgbuild -q > /dev/null
    EC=$?
    if [ $EC != 0 ]; then
        echo "Build failed. Aborting"
        return $EC
    fi
    shift 1
    source $DGCODE_DIR/install/setup.sh && $prog "$@"
}

if [ -f "$DGCODE_DIR/install/setup.sh" ]; then
    source "$DGCODE_DIR/install/setup.sh"
fi

if [ "x$DGCODE_BOOTSTRAP_QUIET" != "x1" ]; then
    echo
    echo "Successfully bootstrapped the ESS Detector group code in: $DGCODE_DIR"
    echo
    echo "You now have the command 'dgbuild' available which you can run from any"
    echo "directory below $DGCODE_DIR to build the software."
    echo
    echo "For more info run: dgbuild --help"
    echo
fi
