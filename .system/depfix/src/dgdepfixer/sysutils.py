
from dgdepfixer.std import *
#from dgdepfixer import allmods as DG

quote = shlex.quote

def chmod_x(path):
    """python equivalent of 'chmod +x'. This version always sets +x for all of user, group, others."""
    path.chmod(path.stat().st_mode | stat.S_IXOTH | stat.S_IXGRP | stat.S_IEXEC )

def mkdir_p(path,*args):
    """python equivalent of 'mkdir -p ...'"""
    if args:
        mkdir_p(path)
        for a in args:
            mkdir_p(a)
        return
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

def rm_rf(path):
    #First attempt with os.remove, and only in case it was a directory go for
    #shutil.rmtree (since shutil.rmtree prints error messages if fed symlinks to
    #directories):
    try:
        os.remove(path)
    except OSError as e:
        if e.errno == errno.ENOENT:#no such file or directory
            return#not an error, rm -rf should exit silently here
        elif e.errno != errno.EISDIR and e.errno!=errno.EPERM:
            raise
    #is apparently a directory
    try:
        shutil.rmtree(path)
    except OSError as e:
        if e.errno == errno.ENOENT:
            return#Sudden disappearance is still ok.
        raise

def isemptydir(path):
    if not hasattr(path,'is_dir'):
        return isemptydir(pathlib.Path(path))
    return path.is_dir() and not any(True for _ in path.iterdir())

def is_executable_file(p):
    """returns True if path is to a file which user can execute (i.e. it must
       exist, be readable and executable, and not be a directory).
    """
    try:
        return p.exists() and not p.is_dir() and os.access(str(p.absolute()), os.X_OK|os.R_OK)
    except PermissionError:
        return False

def quote_cmd(cmd):
    if isinstance(cmd,list):
        return ' '.join(quote(c) for c in cmd)
    return cmd

def system(cmd,catch_output=False):
    """A better alternative to os.system which flushes stdout/stderr, makes sure the
       shell is always bash, and wraps exit codes larger than 127 to 127. Set
       catch_output to True to instead return both exit code and the output of the
       command in a string."""
    #flush output, to avoid confusing ordering in log-files:
    sys.stdout.flush()
    sys.stderr.flush()
    #rather than os.system, we call "bash -c <cmd>" explicitly through
    #the subprocess module, making sure we can always use bash syntax:
    cmd=['bash','-c',cmd]
    #wrap exit code to 0..127, in case the return code is passed on to
    #sys.exit(ec):
    fixec = lambda ec : ec if (ec>=0 and ec<=127) else 127
    if catch_output:
        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output = p.communicate()[0]
        except BaseException:
            #todo: in case of failure we should return the output as well!
            return 1,''
        return fixec(p.returncode),output
    else:
        ec=subprocess.call(cmd)
    sys.stdout.flush()
    sys.stderr.flush()
    return fixec(ec)

def system_throw(cmd,catch_output=False):
    """same as system except doesn't return exit code and throws RuntimeError in case of non-zero exit code"""
    out=None
    if catch_output:
        ec,out = system(cmd,catch_output)
    else:
        ec = system(cmd,catch_output)
    if ec:
        raise RuntimeError('command failed: %s'%cmd)
    return out

def download(url, filename):
    #NB: Synch with one in dgdepfixer script

    filename = str(filename) if isinstance(filename,pathlib.Path) else filename
    start_time=time.time()
    def reporthook(count, block_size, total_size):
        duration = time.time() - start_time
        progress_size = count * block_size
        speed = round( progress_size / (1024. * duration))
        percent = max(0.0,min(100.0,(count * block_size * 100.0 / total_size)))
        min(int(count*block_size*100/total_size),100)
        s="%.1f%%, %.1f MB, %d KB/s, %ds" %(percent, progress_size / (1024. * 1024), speed, duration)
        print("\rDownloading %s [%s]    \r"%(url,s),end='')
    print()
    try:
        urllib.request.urlretrieve(url, filename, reporthook)
    except urllib.error.HTTPError as e:
        print("!!! Problems downloading %s\n"%url)
        raise e

#AbsPath: Return absolute path object from existing or non-existing path : Avoid Path.absolute() calls, as discussed on:
#  https://stackoverflow.com/questions/42513056/how-to-get-absolute-path-of-a-pathlib-path-object
#  https://bugs.python.org/issue29688
if sys.version_info[0:2]<=(3,5):
    AbsPath = lambda p: pathlib.Path(os.path.abspath(str(pathlib.Path(p).expanduser())))
    ResolvedAbsPath = lambda p: AbsPath(p).resolve()
else:
    AbsPath = lambda p: pathlib.Path(p).expanduser().resolve(strict=False)
    ResolvedAbsPath = lambda p: pathlib.Path(p).expanduser().resolve(strict=True)
def SafeResolvedAbsPath(p):
    """ResolvedAbsPath(p) if exists, otherwise just AbsPath(p)"""
    ap = AbsPath(p)
    return ap.resolve() if ap.exists() else ap
AbsPathOrNone = lambda p : (AbsPath(p) if p else None)
