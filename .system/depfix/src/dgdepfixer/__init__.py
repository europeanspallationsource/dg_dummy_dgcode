__author__ = "Thomas Kittelmann"
__copyright__ = "Copyright 2019"
__version__ = "0.0.4"#Important: Update this whenever any significant changes occur that might affect the installations created with --create
__maintainer__ = "Thomas Kittelmann"
__email__ = 'es.ssse@nnamlettik.samoht'[::-1]

swversions={
    #recommended versions for all deps are typically set to the most recent production release at the time of testing.
    'CMake': dict(oldest='3.12.3',recommended='3.14.4',newest=None),#v3.8.2 supports C++17, v3.12.4 supports FindPython3 and C++20. v3.12.3 comes with Conda, we hope it is good enough.
#FIXMEFIXME    'mercurial':dict(oldest='2.6.2',recommended='4.9.1',newest=None),#v2.6.2 is available out of the box on lxplus and DMSC.
    'pymod:numpy':dict(oldest='1.12.0',recommended='1.16.3'),#latest pandas needs numpy 1.12.0, so seems like a reasonable choice.
    'pymod:matplotlib':dict(oldest='1.5.0',recommended='3.0.3'),#will move to oldest=3.0.0 in the near future to lighten maintenance
    'pymod:scipy':dict(oldest='1.0.0',recommended='1.2.1'),#oldest=1.0.0 is pure guess-work
    'pymod:pandas':dict(oldest='0.24.0',recommended='0.24.2'),#oldest=recommended for now since we hardly use pandas
    'pymod:mcpl':dict(oldest='1.2.1',recommended='1.2.1'),#oldest=recommended since we don't want people to use outdated MCPL.
    'pymod:ptpython':dict(oldest='2.0.4',recommended='2.0.4'),#oldest=recommended for now due to lack of validation
    #TODO: Other interesting ones: 'h5py', 'PyNE','pyqt5', ipython, ptpython,...
}
