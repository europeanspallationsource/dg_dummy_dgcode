
from dgdepfixer.std import *
from dgdepfixer import allmods as DG

class OSGInstaller(DG.installutils.stdinstaller):
    def name(self): return 'OpenSceneGraph'
    def default_version(self): return '3.4.1'
    def download_url(self,version):
        return 'https://github.com/openscenegraph/OpenSceneGraph/archive/OpenSceneGraph-%s.zip'%version
    def unpack_command(self,version):
        assert version
        return ['python3','-mzipfile','-e','OpenSceneGraph-%s.zip'%version,'.']
    def src_work_subdir(self,version):
        return 'OpenSceneGraph*-%s'%version
    def in_source_buildandconfig(self): return False
    def dbg_flags(self): return ['-DCMAKE_BUILD_TYPE=Debug','-DCMAKE_DEBUG_POSTFIX=']
    def configure_command(self,instdir,srcdir,blddir,version,extra_options):
        flags=['-DCMAKE_INSTALL_PREFIX=%s'%instdir,
               '-Wno-dev',
               '-DCMAKE_CXX_STANDARD=17',
               '-DCMAKE_BUILD_TYPE=Release',
               '-DLIB_POSTFIX=']
        if not any('CMAKE_DISABLE_FIND_PACKAGE_JPEG' in eo for eo in extra_options):
            #Avoid libjpeg dependency due to incompatibility between recent libjpeg and osg (related to DGSW-385):
            flags += ['-DCMAKE_DISABLE_FIND_PACKAGE_JPEG=ON']
        if sys.platform=='darwin':
            flags+=['-DCMAKE_INSTALL_NAME_DIR=%s/lib'%instdir]
        flags+=extra_options
        return ['cmake']+self._prune_duplicate_flags(flags)+[str(srcdir)]
    def validate_version(self,version):
        parts=version.split('.')
        if len(parts)!=3: return False
        for p in parts[0:2]:
            if not p.isdigit():
                return False
        return True
    def allow_local_source(self): return True
    def local_source_to_version(self,srcfile_basename):
        b,e='OpenSceneGraph-','.zip'
        if srcfile_basename.startswith('OpenSceneGraph-OpenSceneGraph-'):
            b='OpenSceneGraph-OpenSceneGraph-'#work with githubs oddly named files
        if not srcfile_basename.startswith(b): return None
        if not srcfile_basename.endswith(e): return None
        return (srcfile_basename[len(b):])[:-len(e)]
    def setup_file_contents(self,instdir):
        return ['export OSGDIR=%s'%instdir,
                'export LD_LIBRARY_PATH=$OSGDIR/lib64:$OSGDIR/lib:$LD_LIBRARY_PATH',
                'export DYLD_LIBRARY_PATH=$OSGDIR/lib64:$OSGDIR/lib:$DYLD_LIBRARY_PATH',
                'export PATH=$OSGDIR/bin:$PATH']
    def unsetup_file_contents(self,instdir):
        return ['if [ "x$OSGDIR" == "x%s" ]; then'%instdir,
                '    prunepath PATH "$OSGDIR"',
                '    prunepath LD_LIBRARY_PATH "$OSGDIR"',
                '    prunepath DYLD_LIBRARY_PATH "$OSGDIR"',
                '    unset OSGDIR',
                '    unset OSG_DIR',
                '    unset OSG_ROOT',
                'fi']
    def prefix_env_var(self): return 'OSGDIR'
    def libdirs_in_installation(self): return []

def main():
    OSGInstaller().go()
if __name__=='__main__':
    main()


