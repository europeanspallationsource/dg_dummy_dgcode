#pseudo-external which is intended to be present only for C++ compilers which
#can be expected to implement a "reasonably complete" support for C++11.

set(HAS_CXX11 0)

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 6.99)
    set(HAS_CXX11 1)
  endif()
endif()

if (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 4.8.2)
    set(HAS_CXX11 1)
  endif()
endif()

if (HAS_CXX11)
  set(ExtDep_CXX11_COMPILE_FLAGS "")#no need to set -std=c++11 since we anyway have it on by default
  set(ExtDep_CXX11_LINK_FLAGS "")
  set(ExtDep_CXX11_VERSION "-")#a version here doesn't really make sense

endif()

