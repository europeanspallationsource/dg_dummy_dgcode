#!/bin/bash
set -e
echo "dmscjenkinx.x: Bootstrapping (. bootstrap.sh)"
. ./bootstrap.sh
echo "dmscjenkinx.x: Force recfg and build all packages (dgbuild -ae)"
dgbuild -ae
echo "dmscjenkinx.x: Run tests (dgbuild -t)"
dgbuild -t
echo "dmscjenkinx.x: Dumping resulting .bld/testresults/dgtest_results_junitformat.xml"
cat .bld/testresults/dgtest_results_junitformat.xml
